# Using tags to control running of specific tests
```
########################################################
# 
  - name: KUBERNETES  mkdir -p $HOME/.kube
    file:
      path: $HOME/.kube
      state: directory
      mode: '0755'
    tags:
      - jamtest
#
# ansible-playbook  ./playbook.yml --tags "jamtest"
########################################################
```


# Running the playbooks .
```
ansible-playbook -i scs.inventory ./playbook-ping.yml
ansible-playbook -i scs.inventory -l workers ./playbook-ping.yml

ansible-playbook -i scs.inventory -l masters scs-engineer.yml
ansible-playbook -i scs.inventory -l workers scs-engineer.yml
ansible-playbook -i scs.inventory scs-engineer.yml

ansible-playbook -i scs.inventory -l master  k8s-install.yml
ansible-playbook -i scs.inventory -l workers  k8s-install.yml
ansible-playbook -i scs.inventory k8s-install.yml

ansible-playbook -i scs.inventory -l workers fw-services.yml
ansible-playbook -i scs.inventory -l master fw-services.yml
ansible-playbook -i scs.inventory fw-services.yml
```
